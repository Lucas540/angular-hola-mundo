import { Component } from '@angular/core';

@Component({
  /* ES COMO <app-root></app-root> */
  selector: 'app-header',
  /* templateUrl: para importar el html */
  templateUrl: './header.componet.html'
})
/* CUANDO SON POCAS LINEAS DE CODIGO SE PUEDE ESCRIBIR ALLI MISMO
SIN NECESIDAD DE CREAR UN ARCHIVO APARTE (OPCIONAL), SIM EMBARGO SE
PUEDE CREAR Y AGREGAR FUNCIONARA DE LA MISMA MANERA */
/* PARTE DEL Component */
// template: `<h1>Header Component</h1>`

export class HeaderComponent {

}
